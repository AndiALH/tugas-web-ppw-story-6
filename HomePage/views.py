from django.shortcuts import render, redirect
from .forms import Status_Form
from .models import Status

# Create your views here.
def index(request):
    response = {
        'form' : Status_Form,
        'status' : Status.objects.all(),
    }
    return render(request,'index.html', response)

def send(request):
    form = Status_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # return redirect('HomePage:index')
            return redirect('/')

    # response = {
    #     'form' : form
    # }

    # return render(request, 'index.html')