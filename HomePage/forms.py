from django import forms
from .models import Status

class Status_Form(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        labels = {
            'name' : "My Status    :", 
        }
        widgets = {
            'status' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'size' : 70,
                }
            ),
        }