from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve
from . import views
from .models import Status
from .forms import Status_Form
from django.utils import timezone
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class HomePageTest(TestCase):
    def test_homepage_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status='Syaayaa')

        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)
        self.assertEqual(str(new_status), new_status.status)

    def test_can_save_POST_request(self):
        response = self.client.post('/add_status/', data={'status' : 'anjay mabar'})
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('anjay mabar', html_response)


class StatusInputTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')        
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusInputTest, self).setUp()

    def test_can_fill_a_form_read_the_output_and_check_if_it_match(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/'))

        time.sleep(2)
        assert 'test -------- 1234567890' not in selenium.page_source

        status_field = selenium.find_element_by_name('status')
        status_field.send_keys('test -------- 1234567890')

        time.sleep(2)
        status_field.submit()

        time.sleep(3)
        selenium.get('%s%s' % (self.live_server_url, '/'))
        assert 'test -------- 1234567890' in selenium.page_source
        time.sleep(1)
    
    def tearDown(self):
        self.selenium.quit()
        super(StatusInputTest, self).tearDown()





# class StatusInputTest(unittest.TestCase):
#     def setUp(self):
#         self.browser = webdriver.Chrome(ChromeDriverManager().install())

#     def test_bot_can_fill_a_form_and_read_the_output_and_check_it_if_it_match(self):
#         self.browser.get('http://localhost:8000/')
#         time.sleep(2)
#         status_field = self.browser.find_element_by_name('status')
#         status_field.send_keys('test -------- 1234567890')
#         time.sleep(2)
#         status_field.submit()
#         time.sleep(3)
#         self.assertIn("test -------- 1234567890", self.browser.page_source)
#         time.sleep(1)
    
#     def tearDown(self):
#         self.browser.quit()

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')



        

