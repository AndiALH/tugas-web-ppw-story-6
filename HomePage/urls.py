from django.urls import path
from . import views

app_name = 'HomePage'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_status/', views.send, name='add_status')
]