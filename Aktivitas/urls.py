from django.urls import path
from . import views

app_name = 'Aktivitas'

urlpatterns = [
    path('', views.activity, name='aktivitas'),
]