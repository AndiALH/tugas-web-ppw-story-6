from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve
from . import views
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class ActivityTest(TestCase):
    def test_aktivitas_is_exist(self):
        response = Client().get('/Aktivitas/')
        self.assertEqual(response.status_code, 200)

    def test_aktivitas_using_index_template(self):
        response = Client().get('/Aktivitas/')
        self.assertTemplateUsed(response, 'aktivitas.html')

    def test_homepage_using_index_func(self):
        found = resolve('/Aktivitas/')
        self.assertEqual(found.func, views.activity)

class AktivitasPageTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')        
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AktivitasPageTest, self).setUp()

    def test_can_click_the_nightmode_switch(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/Aktivitas/'))

        time.sleep(2)
        darkmode_switch = selenium.find_element_by_id('darkmode-switch')
        darkmode_switch.click()

        time.sleep(2)
        # assert 'test -------- 1234567890' in selenium.page_source
        # background = selenium.find_element_by_tag_name('body')
        # bg_color = background.get_property('background-color')
        # print(bg_color)
        # self.assert
        # time.sleep(1)
    
    def tearDown(self):
        self.selenium.quit()
        super(AktivitasPageTest, self).tearDown()
