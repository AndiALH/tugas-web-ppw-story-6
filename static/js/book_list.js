window.addEventListener("DOMContentLoaded", function (event) {
    if (localStorage.length != 0) {
        let content = [];
        Object.keys(localStorage).forEach(function (key) {
            let item = JSON.parse(localStorage.getItem(key));
            content.push(`<tr>
            <td>${item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="max-width: 18rem" alt="...">` : `<img src="https://icon-library.net/images/not-found-icon/not-found-icon-28.jpg" class="card-img-top" style="max-width: 18rem" alt="...">`}</td>
                
            <td><a>${item.volumeInfo.title}</a></td>
            <td><p>${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "Author not found"}</p></td>
            <td><a class="btn btn-dark" href="https://books.google.co.id/books?id=${item.id}">details</a></td>
            </tr>`)
        });
        document.querySelector(".body").innerHTML = '<div class="card-columns">' + content.join(" ") + '</div>';
    }
})

document.getElementById("search-form").addEventListener("submit", function (event) {
    event.preventDefault();
    document.querySelector(".body").innerHTML = '<div class="loader"></div>'
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            try {
                localStorage.clear()
            } catch (err) {
                console.log("local storage error")
            }
            if (JSON.parse(this.response).totalItems == 0) {
                document.querySelector(".body").innerHTML = "None";
            } else {
                document.querySelector(".body").innerHTML = '<div class="card-columns">' + JSON.parse(this.response).items.map((item, index, itemsArray) => {
                    try {
                        localStorage.setItem(item.id, JSON.stringify(item));
                    } catch (err) {
                        console.log("local storage error")
                    }
                    return `<tr>
                    <td>${item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="max-width: 18rem" alt="...">` : `<img src="https://icon-library.net/images/not-found-icon/not-found-icon-28.jpg" class="card-img-top" style="max-width: 18rem" alt="...">`}</td>
                        
                    <td><a>${item.volumeInfo.title}</a></td>
                    <td><p>${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "Author not found"}</p></td>
                    <td><a class="btn btn-dark" href="https://books.google.co.id/books?id=${item.id}">details</a></td>
                    </tr>`;
                }).join(" ") + '</div>';
            }
        } else if (this.readyState == 4 && (this.status == 400 || this.status == 404)) {
            document.querySelector(".body").innerHTML = "Error 404"
        }
    };
    param = document.getElementById("id_input_book").value.trim().split(" ").join("_");
    xhttp.open("GET", `search_books/${param}/`, true);
    xhttp.setRequestHeader("X-CSRFToken", document.getElementsByName('csrfmiddlewaretoken')[0].value);
    xhttp.send();
})
