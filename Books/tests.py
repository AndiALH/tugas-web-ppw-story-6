from django.test import TestCase, Client
from django.urls import resolve
from . import views
import json
import requests
from django.http import JsonResponse

# Create your tests here.
# Create your tests here.
class BooksTest(TestCase):
    def test_books_is_exist(self):
        response = Client().get('/Books/')
        self.assertEqual(response.status_code, 200)

    def test_books_using_index_func(self):
        found = resolve('/Books/')
        self.assertEqual(found.func, views.books)

    def test_books_using_index_template(self):
        response = Client().get('/Books/')
        self.assertTemplateUsed(response, 'books.html')

    # def test_booklist_is_parsed(self):
    #     response = self.client.get('/find_book/ninja_get_good/')
    #     self.assertEqual(response.status_code, 200)
    #     json = requests.get("https://www.googleapis.com/books/v1/volumes?q=ninja_get_good").json()
    #     self.assertJSONEqual(str(response.content, encoding="utf8"),json)

    # def test_Books_has_content(self):
    #     request = HttpRequest()
    #     engine = import_module(settings.SESSION_ENGINE)
    #     request_session = engine.SessionStore(None)
    #     response = tugas8(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn("Books Finder", html_response)

    

