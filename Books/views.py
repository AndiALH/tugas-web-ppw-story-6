from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
from .forms import inputbook

# Create your views here.
def books(request):
    context = {
        'form' : inputbook(),
    }
    return render(request,'books.html', context)
    # return render(request,'books.html')

def search_books(request, param):
    json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + param).json()
    return JsonResponse(json)
