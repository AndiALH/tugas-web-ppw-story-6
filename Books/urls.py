from django.urls import path
from . import views

app_name = 'Books'

urlpatterns = [
    path('', views.books, name='books'),
    path('search_books/<str:param>/', views.search_books, name='search_books'),
]